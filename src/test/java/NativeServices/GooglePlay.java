package NativeServices;

import SystemSupport.Screenshoter;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

public class GooglePlay {
  public  AppiumDriver driver;
    public GooglePlay(AppiumDriver driver){
        this.driver=driver;
    }
    public void loginAccount(){

    }
    public void clickBuy() {
        try {

            driver.tap(1, driver.findElement(By.xpath("//android.widget.Button[contains(@text,'BUY')and @index=0]")), 10);
        } catch (NoSuchElementException e) {
            new Screenshoter(driver).saveScreenshot();
        }
    }
}
