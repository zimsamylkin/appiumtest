import ElementsUpperWorld.AbstractUpperWorldElement;
import MenuAndPanels.Menu;
import MenuAndPanels.Panel;
import NativeServices.GooglePlay;
import Popups.AchivPopup;
import Popups.FBLoginPopup;
import Popups.MoneyPopup;
import Popups.PickPopup;
import SystemSupport.Data.*;
import SystemSupport.ReaderProperty;
import SystemSupport.SupportClass;
import org.aspectj.lang.annotation.Before;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.io.IOException;
import java.net.MalformedURLException;


@Title("All test")
public class RunnnerTest  extends SupportClass {

    @BeforeMethod
    public void init() throws IOException {
        getAPKName();
      //  serverDefaultStart();
      //  serverStart(4444);
        try {
            initDriver(ReaderProperty.takeData(),"http://127.0.0.1:4444/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @AfterMethod
    public void end() {
        quitDriver();
     //   server.stop();
    }


    @Title("smokeTest")
    @Description("install,open,wait main menu,delete")
    @Test
    @Severity(SeverityLevel.CRITICAL)
   // @Test
    public void smokeTest() {
        Application application = new Application(driver);
        application.install(PATH_TO_FULL_APK);
        application.open();
        application.closed();
        application.uninstall();
    }

    @Title("test main menu")
    @Description("click all buttons in main menu")
    @Test
    @Severity(SeverityLevel.CRITICAL)

    public void mainPageTest(String FOLDER_SCREENS) {
        MenuElements menuElements=new MenuElements(FOLDER_SCREENS);
        Application application = new Application(driver);
        Menu mainMenu = new Menu(driver, OCR,
                new MenuElements(FOLDER_SCREENS),
                new Buttons(FOLDER_SCREENS),
                new LowWorldElements(FOLDER_SCREENS)
        );
        application.install(PATH_TO_FULL_APK);
        application.open();
        mainMenu.clickPlay();
        application.open();
        mainMenu.clickLoginButton();
        mainMenu.clickRateIt();
        mainMenu.clickAchiveOrResults(menuElements.getAchivButton());
        mainMenu.clickAchiveOrResults(menuElements.getResultsButton());
        mainMenu.clickSupport();
        mainMenu.clickFBorVK(menuElements.getVkButton());
        mainMenu.clickFBorVK(menuElements.getFbButton());
    }

   @Title("test login")
   @Description("login,logout,take localdata")
    @Test
    @Severity(SeverityLevel.NORMAL)

    public void loginTest(String FOLDER_SCREENS) {// TODO динамичные икспасы
        MenuElements menuElements=new MenuElements(FOLDER_SCREENS);
        Application application = new Application(driver);
        FBLoginPopup fbLogin = new FBLoginPopup(driver, OCR,
                new MenuElements(FOLDER_SCREENS),
                new Popups(FOLDER_SCREENS));
        application.install(PATH_TO_FULL_APK);
        application.open();
        fbLogin.openPopup(menuElements.getLoginButton());
        fbLogin.waitPopup();
        fbLogin.enterTestData();
        fbLogin.enterLocalData();
        fbLogin.logout();
    }

    @Title("test tutorial")
    @Description("play tutorial and go to upper world")
    @Severity(SeverityLevel.CRITICAL)
    @Test


    public void tutorialTest(String FOLDER_SCREENS) throws InterruptedException {
        MenuElements menuElements=new MenuElements(FOLDER_SCREENS);
        Application application = new Application(driver);
        Player player = new Player(driver, OCR,menuElements,new Buttons(FOLDER_SCREENS));
        Menu menu = new Menu(driver, OCR,
                new MenuElements(FOLDER_SCREENS),
                new Buttons(FOLDER_SCREENS),
                new LowWorldElements(FOLDER_SCREENS)
                 );
        application.install(PATH_TO_FULL_APK);
        application.open();
        player.startGame();
        player.sizeForSwipe();
        player.walkToTutorial();
        player.enterMenu();
        menu.endGame();
    }

    @Title("test game menu in low world")
    @Severity(SeverityLevel.NORMAL)
    @Test

    public void lowGameMenuTest(String FOLDER_SCREENS) throws InterruptedException {//TODO проверить
        MenuElements menuElements=new MenuElements(FOLDER_SCREENS);
        Player player = new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        Menu menu = new Menu(driver, OCR,
                new MenuElements(FOLDER_SCREENS),
                new Buttons(FOLDER_SCREENS),
                new LowWorldElements(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        player.sizeForSwipe();
        Thread.sleep(Timeouts.getTimeoutSkipMission());
        player.swipeDown();
        player.enterMenu();
        menu.offMusic();
        menu.onMusic();
        menu.offSource();
        menu.onSource();
        menu.clickAchiveOrResults(menuElements.getAchivButton());
        menu.clickAchiveOrResults(menuElements.getResultsButton());
        menu.clickEndAndNo();
        menu.clickExitMenu();
    }

    @Title("test game menu in upper world")
    @Severity(SeverityLevel.NORMAL)
    @Test

    public void upperWorldMenuTest(String FOLDER_SCREENS) throws InterruptedException {
        MenuElements menuElements=new MenuElements(FOLDER_SCREENS);
        Menu upperMenu = new Menu(driver, OCR,
                new MenuElements(FOLDER_SCREENS),
                new Buttons(FOLDER_SCREENS),
                new LowWorldElements(FOLDER_SCREENS));
        Player player = new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        Thread.sleep(6000);
        player.enterMenu();
        upperMenu.offMusic();
        upperMenu.onMusic();
        upperMenu.offSource();
        upperMenu.onSource();
        upperMenu.clickAchiveOrResults(menuElements.getAchivButton());
        upperMenu.clickAchiveOrResults(menuElements.getResultsButton());
        upperMenu.clickFBorVK(menuElements.getVkButton());
    }

   @Title("test blacksmith")
    @Description("only view and go to popup")
    @Test

    public void blacksmithTest(String FOLDER_SCREENS) throws InterruptedException {
        UpperWorldElements upperWorldElements=new UpperWorldElements(FOLDER_SCREENS);
        AbstractUpperWorldElement blacksmith = new AbstractUpperWorldElement(driver, OCR,
                new UpperWorldElements(FOLDER_SCREENS));
        Player player = new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        PickPopup pickPopup = new PickPopup(driver, OCR,
                new Buttons(FOLDER_SCREENS),
                new Popups(FOLDER_SCREENS),
                new Scins(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        blacksmith.waitElement(upperWorldElements.getBLACKSMITH());
        blacksmith.view(upperWorldElements.getBLACKSMITH());
        blacksmith.view(upperWorldElements.getActiveSpeechbuble());
        player.sizeForSwipe();
        player.shortSwipeRight();
        //player.swipeRight();
        player.swipeUp();
        pickPopup.view();
    }

    @Title("test oldman")
    @Description("only view")
    @Test

    public void oldmanTest(String FOLDER_SCREENS) throws InterruptedException {
        UpperWorldElements upperWorldElements=new UpperWorldElements(FOLDER_SCREENS);
        AbstractUpperWorldElement oldMan = new AbstractUpperWorldElement(driver, OCR,
                new UpperWorldElements(FOLDER_SCREENS));
        Player player = new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        player.sizeForSwipe();
        player.skipMission();
        oldMan.waitElement(upperWorldElements.getOLDMAN());
    }

    @Title("test achivStone")
    @Description("view and go to popup")
    @Test

    public void achivStoneTest(String FOLDER_SCREENS) throws InterruptedException {
        UpperWorldElements upperWorldElements=new UpperWorldElements(FOLDER_SCREENS);
        Player player = new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        AbstractUpperWorldElement stone = new AbstractUpperWorldElement(driver, OCR,
                new UpperWorldElements(FOLDER_SCREENS));
        AchivPopup popup = new AchivPopup(driver, OCR,
                new Popups(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        player.sizeForSwipe();
        player.skipMission();
        player.skipMission();
        stone.waitElement(upperWorldElements.getAchivStone());
        stone.view(upperWorldElements.getAchivStone());
        player.shortSwipeLeft();
        player.shortSwipeLeft();
        player.swipeUp();
        popup.view();

    }

    @Title("test panel in upper world")
    @Description("test click and go to pickpopup,moneypopup")
    @Test
    public void upperWorldPanelTest(String FOLDER_SCREENS) throws InterruptedException {
        PanelPath panelPath=new PanelPath(FOLDER_SCREENS);
        Player player = new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        Panel panel = new Panel(driver, OCR,
                new PanelPath(FOLDER_SCREENS));
        MoneyPopup moneyPopup = new MoneyPopup(driver, OCR,
                new Popups(FOLDER_SCREENS),
                new Buttons(FOLDER_SCREENS));
        PickPopup pickPopup = new PickPopup(driver, OCR,
                new Buttons(FOLDER_SCREENS),
                new Popups(FOLDER_SCREENS),
                new Scins(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        Thread.sleep(Timeouts.getTimeoutSkipMission());
        panel.enterPickPopup();
        pickPopup.view();
        player.sizeForSwipe();
        player.skipPopup();
        panel.waitElementInPanel(panelPath.getSilverPanelEnterPopup());
        panel.enterSilverCoinsPopup();
        moneyPopup.view();
        player.skipPopup();
        player.skipMission();
        player.skipMission();
        player.skipPopup();
        panel.enterGoldCoinsPopup();
        moneyPopup.view();
    }

    @Title("test banners in money popup")
    @Description("test view and open banners")
    @Test
    public void bannerMoneyPopupTest(String FOLDER_SCREENS) throws InterruptedException {
        PanelPath panelPath=new PanelPath(FOLDER_SCREENS);
        Banners banners=new Banners(FOLDER_SCREENS);
        Popups popups=new Popups(FOLDER_SCREENS);
        Panel panel = new Panel(driver, OCR,
                new PanelPath(FOLDER_SCREENS));
        Player player=new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        MoneyPopup popup = new MoneyPopup(driver, OCR,
                new Popups(FOLDER_SCREENS),
                new Buttons(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        panel.waitElementInPanel(panelPath.getSilverPanelEnterPopup());
        panel.enterSilverCoinsPopup();
        popup.openBanner(banners.getGoldFreeBanner());
        player.waitPopup(popups.getGet10GoldPopup());
        popup.skipPopupWithCoins();
        popup.openBanner(banners.getFbMoneyBanner());
        player.goBack();
        player.waitPopup(popups.getGet5GoldPopup());
       popup.skipPopupWithCoins();
        popup.openBanner(banners.getBubleBirdsBunner());
        player.goBack();
        player.waitPopup(popups.getGet2GoldPopup());

    }
    @Title("test upgrade pick")
    @Description("upgrade first and second picks,buy money for upgrade")
    @Test
    public void pickUpgradeTest(String FOLDER_SCREENS) throws InterruptedException {
        GooglePlay googlePlay=new GooglePlay(driver);
        Player player=new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        PickPopup pickPopup=new PickPopup(driver,OCR,
                new Buttons(FOLDER_SCREENS),
                new Popups(FOLDER_SCREENS),
                new Scins(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        System.out.println("!!tutorial");
        Thread.sleep(Timeouts.getTimeoutSkipMission());
        player.sizeForSwipe();
       // player.swipeRight();
        player.shortSwipeRight();
        player.swipeUp();
        pickPopup.view();
        System.out.println("!!popup");
        pickPopup.upgrade();
        System.out.println("!!upgrade");
        pickPopup.takeOldScin();
        System.out.println("!!take old scin");
        pickPopup.upgradeWithoutMoney(googlePlay);
        System.out.println("!!upgrade with buy");
    }

    @Title("test buy money")
    @Test
    public void buyMoneyTest(String FOLDER_SCREENS) throws InterruptedException {//TODO доделать
        ElementsForBuy elementsForBuy=new ElementsForBuy(FOLDER_SCREENS);
        PanelPath panelPath=new PanelPath(FOLDER_SCREENS);
        GooglePlay googlePlay=new GooglePlay(driver);
        Player player=new Player(driver, OCR,new MenuElements(FOLDER_SCREENS),new Buttons(FOLDER_SCREENS));
        Panel panel=new Panel(driver,OCR,new PanelPath(FOLDER_SCREENS));
        MoneyPopup popup=new MoneyPopup(driver,OCR,
                new Popups(FOLDER_SCREENS),
                new Buttons(FOLDER_SCREENS));
        tutorialTest(FOLDER_SCREENS);
        player.sizeForSwipe();
        panel.waitElementInPanel(panelPath.getSilverPanelEnterPopup());
        panel.enterSilverCoinsPopup();
        player.longSwipeUp();
        player.longSwipeUp();
        popup.clickMoreOffers();
        popup.buyELement(elementsForBuy.getSilverBuy100000(),googlePlay);
        popup.buyELement(elementsForBuy.getSilverBuy200000(),googlePlay);
        popup.buyELement(elementsForBuy.getSilverBuy750000(),googlePlay);
        player.swipeUp();
        popup.buyELement(elementsForBuy.getSilverBuy2000000(),googlePlay);
        popup.buyELement(elementsForBuy.getSilverBuy5000000(),googlePlay);
        popup.buyELement(elementsForBuy.getSilverBuy15000000(),googlePlay);
        player.longSwipeUp();
        popup.buyELement(elementsForBuy.getGoldBuy40(),googlePlay);
        popup.buyELement(elementsForBuy.getGoldBuy75(),googlePlay);
        popup.buyELement(elementsForBuy.getGoldBuy150(),googlePlay);
        player.longSwipeUp();
        popup.buyELement(elementsForBuy.getGoldBuy450(),googlePlay);
        popup.buyELement(elementsForBuy.getGoldBuy1500(),googlePlay);
        popup.buyELement(elementsForBuy.getGoldBuy5000(),googlePlay);
        player.longSwipeUp();
        popup.clickBackButton();
    }

}
