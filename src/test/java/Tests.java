import org.testng.annotations.Test;

import javax.swing.*;
import java.io.File;
import java.net.MalformedURLException;

public class Tests extends RunnnerTest implements Runnable {
    String params;
    public Tests(String params){
        this.params=params;
    }
    @Override
    public void run() {
        String[]paramsMassive=params.split(" ");
        try {
            prepareParametrs(paramsMassive);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String SCREENS_DIRECTORY=getScreensDirectory(paramsMassive[4]);
        smokeTest();
     //   try {
       //     tutorialTest(SCREENS_DIRECTORY);
     //   } catch (InterruptedException e) {
      //      e.printStackTrace();
      //  }
      //  mainPageTest(SCREENS_DIRECTORY);
    }
    private void prepareParametrs(String[]paramsMassive) throws MalformedURLException {

        String url="http://127.0.0.1:"+paramsMassive[2]+"/wd/hub";
        getAPKName();
        initDriver(paramsMassive,url);

    }
    private String getScreensDirectory(String size){
         JFileChooser dirObj = new JFileChooser(new File("."));
        String DIR_SCREENS = dirObj.getCurrentDirectory().toString() + "\\screens";
        return  DIR_SCREENS+"\\"+size;

    }
}
