import SystemSupport.Data.Buttons;
import SystemSupport.Data.MenuElements;
import SystemSupport.Data.Timeouts;
import SystemSupport.OCR;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.ElementNotVisibleException;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;

public class Player {
    private ArrayList<Integer> size = new ArrayList<>();
    private AndroidDriver driver;
    private OCR ocr;
    private   MenuElements menuElements;
    private  Buttons buttons;

    public Player(AndroidDriver driver, OCR ocr,MenuElements menuElements,Buttons buttons) {
        this.driver = driver;
        this.ocr = ocr;
        this.menuElements=menuElements;
        this.buttons=buttons;

    }

    private static double coeffStartX=0.2;
    private static double coeffEndX=0.268;
    private static double coeffStartY=0.51;
    private static double coeffEndY=0.458;
    private static int speedForSwipe=2000;
    private static int speedForClick=3;
    private static int coeffForSkipPopup=5;

    /*
   Methods for swipe
    */
    @Step("swipe up")
    public void swipeUp() throws InterruptedException {
        Thread.sleep(Timeouts.getTimeoutForSwipe());
        driver.swipe(size.get(0), size.get(2), size.get(0), size.get(3), 2000);
    }
    @Step("long swipe up")
    public void longSwipeUp() throws InterruptedException {
        Thread.sleep(Timeouts.getTimeoutForSwipe());
        driver.swipe(size.get(0),size.get(3)*2,size.get(0),size.get(2),2000);
    }
    @Step("swipe down")
    public void swipeDown() throws InterruptedException {
        Thread.sleep(Timeouts.getTimeoutForSwipe());
        driver.swipe(size.get(0), size.get(3), size.get(0), size.get(2), 2000);
    }
    @Step("swipe right")
    public void swipeRight() throws InterruptedException {
        Thread.sleep(Timeouts.getTimeoutForSwipe());
        driver.swipe(size.get(0), size.get(3), size.get(1), size.get(3), 2000);
    }
    @Step("swipe left")
    public void swipeLeft() throws InterruptedException {
        Thread.sleep(Timeouts.getTimeoutForSwipe());
        driver.swipe(size.get(1), size.get(3), size.get(0), size.get(3),2000);
    }
    @Step("short swipe left")
    public void shortSwipeLeft() throws InterruptedException {
        Thread.sleep(Timeouts.getTimeoutForSwipe());
        driver.swipe(size.get(1)-size.get(0)/6, size.get(3), size.get(0), size.get(3),2000);
    }
    @Step("short swipe right")
    public void shortSwipeRight() throws InterruptedException {
        Thread.sleep(Timeouts.getTimeoutForSwipe());
        driver.swipe(size.get(0), size.get(3), size.get(1)-size.get(0)/6, size.get(3), 2000);
    }
    void sizeForSwipe() {
        int height = driver.manage().window().getSize().getHeight();
        int width = driver.manage().window().getSize().getWidth();
        size.add((int) (width *coeffStartX));
        size.add((int) (width * coeffEndX));
        size.add((int) (height * coeffStartY));
        size.add((int) (height * coeffEndY));
    }
    /*
    game methods
     */
    @Step("start game")
    void startGame() {

    //    if(ocr.findImage(ConstantData.NO_BUTTON).contains("FIND")){
       //     ocr.clickByImage(ConstantData.NO_BUTTON);//если есть окно "обновите на более новую версию"-жмем нет
        //}
        ocr.clickByImage(menuElements.getPlayButton());
    }
    @Step("play tutorial")
    void walkToTutorial() throws InterruptedException {
        for (int num = 0; num < 3; num++) {
            swipeDown();
        }
        for (int num = 0; num < 1; num++) {
            swipeLeft();
        }
        swipeDown();
        for (int num = 0; num < 4; num++) {
            swipeRight();
        }
        for (int num = 0; num < 2; num++) {
            swipeDown();
        }
        for (int num = 0; num < 2; num++) {
            swipeRight();
        }
        for (int num = 0; num < 2; num++) {
            swipeUp();
        }
        for (int num = 0; num < 1; num++) {
            swipeRight();
        }
        for (int num = 0; num < 3; num++) {
            swipeUp();
            System.out.println("SWIPE: " + num);
        }
        swipeRight();
        swipeUp();
        swipeRight();
        swipeUp();
        for (int num = 0; num < 3; num++) {
            swipeRight();
        }
        for (int num = 0; num < 1; num++) {
            swipeDown();
        }
        swipeRight();
        for (int num = 0; num < 2; num++) {
            swipeDown();
        }
        swipeLeft();
        for (int num = 0; num < 3; num++) {
            swipeDown();
        }
        for (int num = 0; num < 4; num++) {
            swipeRight();
        }
        for (int num = 0; num < 2; num++) {
            swipeDown();
        }
        for (int num = 0; num < 2; num++) {
            swipeRight();
        }
    }
   @Step("go to game menu in low world")
    void enterMenu() {
        ocr.clickByImage(menuElements.getPassivePause());
    }
    /*
    methods for skip
     */
   @Step("skip mission")
    void skipMission() throws InterruptedException {
        Thread.sleep(Timeouts.getTimeoutSkipMission());
        skipPopup();
        swipeDown();
        try {
            ocr.clickByImage(menuElements.getPassivePause());
            ocr.clickByImage(menuElements.getHomeButton());
            ocr.clickByImage(buttons.getYesButton());
        }catch (ElementNotVisibleException e){
            e.printStackTrace();
        }
    }
    @Step("skip popup after new element in upper world")
    void skipPopup() throws InterruptedException {
        for(int num=0;num<5;num++){
            Thread.sleep(Timeouts.getTimeoutSkipPopup());
            driver.tap(2, 1*coeffForSkipPopup,1* coeffForSkipPopup,5);
            driver.tap(2,129*coeffForSkipPopup,101*coeffForSkipPopup,5);
        }
    }
    @Step("go to application again")
    public void goBack(){
       driver.pressKeyCode(AndroidKeyCode.BACK);
    }
    @Step("wait popup")
    public void waitPopup(String popupPath){
        ocr.waitUntilImageExists(popupPath,1000);
    }
}
