package ElementsUpperWorld;

import SystemSupport.Data.UpperWorldElements;
import SystemSupport.OCR;
import io.appium.java_client.AppiumDriver;
import ru.yandex.qatools.allure.annotations.Step;

import static org.testng.AssertJUnit.assertFalse;

public class AbstractUpperWorldElement {


    public AppiumDriver driver;
    public OCR ocr;
    UpperWorldElements upperWorldElements;
    public AbstractUpperWorldElement(AppiumDriver driver,OCR ocr, UpperWorldElements upperWorldElements){
        this.driver=driver;
        this.ocr=ocr;
        this.upperWorldElements=upperWorldElements;
    }


    @Step("test view element")
  public  void view(String elementName){
            assertFalse(ocr.findImage(elementName).contains("NOT VISIBLE"));
    }
  public void activeSpichBable(){
      assertFalse(ocr.findImage(upperWorldElements.getActiveSpeechbuble()).contains("NOT VISIBLE"));
  }
  @Step("wait element")
    public void waitElement(String elementName){
      ocr.waitUntilImageExists(elementName,100);
  }//TODO вернуть
}
