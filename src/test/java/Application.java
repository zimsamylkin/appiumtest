import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.Connection;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

class Application {

    private AndroidDriver driver;
    Application(AndroidDriver driver){
        this.driver=driver;
    }
    @Step("install application")
    void install(String pathToApk){
        driver.installApp(pathToApk);
    }
    @Step("uninstall application")
    void uninstall(){
        driver.removeApp("com.bandagames.miner");
    }

    @Step("open application")
    void open() {
        driver.launchApp();
        WebElement explicitWait = ((new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.RelativeLayout"))));

    }
    @Step("close application")
    void closed(){
        driver.closeApp();
    }
    void closeNetConnection(){
        driver.setConnection(Connection.NONE);
    }
    void openNetConnection(){
        driver.setConnection(Connection.ALL);
    }

}
