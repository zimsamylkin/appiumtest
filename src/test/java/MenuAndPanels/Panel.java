package MenuAndPanels;

import SystemSupport.Data.PanelPath;
import SystemSupport.OCR;
import io.appium.java_client.android.AndroidDriver;
import ru.yandex.qatools.allure.annotations.Step;

public class Panel {
    public AndroidDriver driver;
    public OCR ocr;
    PanelPath panelPath;
    public Panel(AndroidDriver driver, OCR ocr,PanelPath panelPath){
        this.driver=driver;
        this.ocr=ocr;
        this.panelPath=panelPath;
    }

    @Step("click to silver element")
    public void enterSilverCoinsPopup() throws InterruptedException {
        Thread.sleep(1000);
             ocr.clickByImage(panelPath.getSilverPanelEnterPopup());
        }
    @Step("click to gold element")
    public void enterGoldCoinsPopup() throws InterruptedException {
        Thread.sleep(1000);
        ocr.clickByImage(panelPath.getGoldPanelEnterPopup());
    }
    @Step("click to pick element")
    public void enterPickPopup() throws InterruptedException {
        Thread.sleep(1000);
            ocr.clickByImage(panelPath.getPickPanelEnterPopup());
    }
    @Step("wait element in panel")
    public void waitElementInPanel(String elementName){
        ocr.waitUntilImageExists(elementName,10000);
    }
}
