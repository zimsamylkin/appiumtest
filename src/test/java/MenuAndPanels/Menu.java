package MenuAndPanels;

import SystemSupport.Data.Buttons;
import SystemSupport.Data.LowWorldElements;
import SystemSupport.Data.MenuElements;
import SystemSupport.Data.Timeouts;
import SystemSupport.OCR;
import SystemSupport.Screenshoter;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

  public class Menu {
    public AndroidDriver driver;
    public OCR ocr;
      MenuElements menuElements;
      Buttons buttons;
      LowWorldElements lowWorldElements;
    public Menu(AndroidDriver driver, OCR ocr,MenuElements menuElements,Buttons buttons,LowWorldElements lowWorldElements){
        this.driver=driver;
        this.ocr=ocr;
        this.menuElements=menuElements;
       this.buttons=buttons;
       this.lowWorldElements=lowWorldElements;
    }
      @Step("click play")
      public void clickPlay(){
        try {
            ocr.clickByImage(menuElements.getPlayButton());
            if (ocr.findImage(buttons.getNoButton()).contains("FIND")) {
                ocr.clickByImage(buttons.getNoButton());//если есть окно "обновите на более новую версию"-жмем нет
            }
            ocr.waitUntilImageExists(lowWorldElements.getScreenTutorial(), Timeouts.getTimeoutWaitWindow());
        }catch (ElementNotVisibleException e) {
            e.printStackTrace();
        }
    }

    @Step("click login")
    public void clickLoginButton(){
          try {
              ocr.clickByImage(menuElements.getLoginButton());
              WebElement explicitWaitLogin = (new WebDriverWait(driver, Timeouts.getTimeoutWaitWindow())).until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.EditText")));//ожидание логин формы
              driver.tap(1, driver.findElement(By.className("android.widget.ImageView")), 2000);
          }catch (ElementNotVisibleException e) {
             new Screenshoter(driver).saveScreenshot();
          }
    }

    @Step("click rate it")
    public void clickRateIt(){
        try {
            String xpathRateTextField = "//android.widget.TextView[contains(@text,'Dig Out!')and @index=1]";
            ocr.clickByImage(menuElements.getRatingButton());
            WebElement explicitWait = (new WebDriverWait(driver, Timeouts.getTimeoutWaitWindow())).until(ExpectedConditions.presenceOfElementLocated(By.id("com.android.vending:id/recycler_view")));//ожидание нативного вью
            WebElement digOutView = (new WebDriverWait(driver, Timeouts.getTimeoutWaitWindow()).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathRateTextField))));//проверка,что вью именно дигаута
            driver.navigate().back();
        }catch (ElementNotVisibleException e) {
            e.printStackTrace();
        }
    }

    @Step("click achiv or results")
    public void clickAchiveOrResults(String button){
        ocr.clickByImage(button);
        try {
            WebElement explicitWaitGoogle = (new WebDriverWait(driver, Timeouts.getTimeoutWaitWindow())).until(ExpectedConditions.presenceOfElementLocated(By.id("com.google.android.play.games:id/swipe_refresh_layout")));
            driver.navigate().back();
        } catch (TimeoutException e) {
            outputGooglePlayError();

        }
    }

      @Step("ERROR:NO GOOGLE PLAY POPUP")
      private void outputGooglePlayError(){
          System.out.println("GOOGLE PLAY NOT WORK");
      }

      @Step("click fb/vk")
    public void clickFBorVK(String button){
          try {
              ocr.clickByImage(button);
              String url = "https://m.vk.com/digout";
              if (button.contains("fb")) {
                  url = "https://m.facebook.com/digout.community/";
              }
              WebElement explicitWaitGoogle = (new WebDriverWait(driver, Timeouts.getTimeoutWaitWindow())).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.EditText[contains(@text,'" + url + "')and @index=1]")));
          }catch (ElementNotVisibleException e) {
              e.printStackTrace();
          }
          driver.pressKeyCode(AndroidKeyCode.BACK);
    }

    @Step("click support")
    public void clickSupport() {
            ocr.clickByImage(menuElements.getSupportButton());
            WebElement explicitWaitSuppor = (new WebDriverWait(driver, Timeouts.getTimeoutWaitWindow())).until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.TextView")));
            driver.navigate().back();
    }


    @Step("exit in menu")
    public void clickExitMenu() {
            ocr.clickByImage(menuElements.getActivePause());
    }

    @Step("on music")
    public void onMusic() {
            ocr.clickByImage(menuElements.getMusicOffButton());
    }

    @Step("off music")
    public void offMusic(){
            ocr.clickByImage(menuElements.getMusicOnButton());
    }

    @Step("on source")
    public void onSource(){
            ocr.clickByImage(menuElements.getSourceOffButton());
    }

    @Step("off source")
    public void offSource(){
            ocr.clickByImage(menuElements.getSourceOnButton());
    }

    @Step("go home(exit) in low game menu")
    public void endGame(){
            ocr.clickByImage(menuElements.getHomeButton());
            ocr.clickByImage(buttons.getYesButton());
   }
   @Step("go home(exit) in low game menu and click cancel")
   public void clickEndAndNo(){
            ocr.clickByImage(menuElements.getHomeButton());
            ocr.clickByImage(buttons.getNoButton());
   }
   @Step("INFORM:VERSION CORRECT")
      public void inform(){
       System.out.println("INFORM:VERSION CORRECT");
   }

}
