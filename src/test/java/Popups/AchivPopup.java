package Popups;

import SystemSupport.Data.Popups;
import SystemSupport.OCR;
import io.appium.java_client.AppiumDriver;
import ru.yandex.qatools.allure.annotations.Step;

import static org.testng.AssertJUnit.assertFalse;


public class AchivPopup {
    AppiumDriver driver;
    OCR ocr;
    Popups popups;
    public AchivPopup(AppiumDriver driver,OCR ocr,Popups popups){
        this.driver=driver;
        this.ocr=ocr;
        this.popups=popups;
    }
    @Step("test view achiv popup")
  public  void view(){
        assertFalse(ocr.findImage(popups.getAchivPopup()).contains("NOT VISIBLE"));
    }
}
