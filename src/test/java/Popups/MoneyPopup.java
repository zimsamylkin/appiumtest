package Popups;

import NativeServices.GooglePlay;
import SystemSupport.Data.Buttons;
import SystemSupport.Data.Popups;
import SystemSupport.Data.Timeouts;
import SystemSupport.OCR;
import io.appium.java_client.AppiumDriver;
import ru.yandex.qatools.allure.annotations.Step;

import static org.testng.AssertJUnit.assertFalse;


public class MoneyPopup {
        AppiumDriver driver;
        OCR ocr;
      Popups popups;
      Buttons buttons;
        public MoneyPopup(AppiumDriver driver,OCR ocr,Popups popups,Buttons buttons){
            this.driver=driver;
            this.ocr=ocr;
            this.popups=popups;
            this.buttons=buttons;
        }

    @Step("test view popup with money")
       public void view(){
                assertFalse(ocr.findImage(popups.getMoneyPopup()).contains("NOT VISIBLE"));

        }
        @Step("open banner")
    public void openBanner(String bannerName){
               ocr.clickByImage(bannerName);
           driver.tap(2,0,0, Timeouts.getTimeoutSkipPopup());//скип попап получения денег
        }

        @Step("skip popup with getting sum")
        public void skipPopupWithCoins(){
            driver.tap(1,driver.manage().window().getSize().getWidth()/2,driver.manage().window().getSize().getHeight()/2,40);
        }
        @Step("go to more offers")
       public void clickMoreOffers(){
            ocr.clickByImage(buttons.getMoreOffersButton());
        }
    @Step("buy")
   public void buyELement(String elementName, GooglePlay googlePlay) {
        ocr.clickByImage(elementName);
        googlePlay.clickBuy();
        ocr.waitUntilImageExists(elementName,10000);
    }
    @Step("go to short view for money")
    public void clickBackButton(){
           ocr.clickByImage(buttons.getBackButtonWhite());
    }
    }
