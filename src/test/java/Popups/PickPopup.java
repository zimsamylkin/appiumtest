package Popups;

import NativeServices.GooglePlay;
import SystemSupport.Data.Buttons;
import SystemSupport.Data.Popups;
import SystemSupport.Data.Scins;
import SystemSupport.OCR;
import com.sun.org.apache.bcel.internal.generic.POP;
import io.appium.java_client.AppiumDriver;
import ru.yandex.qatools.allure.annotations.Step;

import static org.testng.AssertJUnit.assertFalse;


public class PickPopup  {
    AppiumDriver driver;
    OCR ocr;
    Buttons buttons;
    Popups popups;
    Scins scins;
    public PickPopup(AppiumDriver driver,OCR ocr,Buttons buttons,Popups popups,Scins scins){
        this.driver=driver;
        this.ocr=ocr;
        this.buttons=buttons;
        this.popups=popups;
        this.scins=scins;
    }

    @Step("test view popup")
    public void view(){
        assertFalse(ocr.findImage(popups.getPickPopup()).contains("NOT VISIBLE"));
    }
    @Step("upgrade first pick")
    public void upgrade(){
        ocr.clickByImage(buttons.getUpgradeButton());
        ocr.waitUntilImageExists(popups.getSecondPickPopup(),100);
        ocr.clickByImage(buttons.getSelectButton());
        ocr.waitUntilImageExists(scins.getSecondPickScin(),100);
    }
    @Step("take old skin for popup")
    public void takeOldScin(){
        ocr.clickByImage(scins.getSecondPickScin());
        ocr.clickByImage(scins.getFirstPickScin());
        ocr.clickByImage(buttons.getSelectButtonBlackText());
        ocr.findImage(buttons.getCurrentButton());
        driver.tap(1,15,15,100);
        ocr.findImage(scins.getFirstdPickScinRed());
        ocr.findImage(popups.getStrengthSecondPickPoppup());
    }
    @Step("upgrade picks without money")
    public void upgradeWithoutMoney(GooglePlay googlePlay){
        ocr.clickByImage(buttons.getUpgradeButton());
        ocr.clickByImage(buttons.getNoButton());
        ocr.clickByImage(buttons.getUpgradeButton());
        ocr.clickByImage(buttons.getYesButton());
        googlePlay.clickBuy();
        ocr.waitUntilImageExists(buttons.getUpgradeButton(),1000);
        ocr.clickByImage(buttons.getUpgradeButton());
        ocr.clickByImage(buttons.getUpgradeButton());
        ocr.waitUntilImageExists(buttons.getSelectButton(),1000);
    }
}
