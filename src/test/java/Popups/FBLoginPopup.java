package Popups;

import SystemSupport.Data.MenuElements;
import SystemSupport.Data.Popups;
import SystemSupport.Data.Timeouts;
import SystemSupport.OCR;
import SystemSupport.Screenshoter;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

public class FBLoginPopup {
    private String idPasswordField="u_0_1";
    private String idLoginInPopup="u_0_5";
    private String idContinuePopup="m-future-page-header-title";
    private String idContinueButton="u_0_3";
     private String xpathEmailField="//android.widget.EditText[contains(@text,'Email address or phone number')and @index=0]";
     private String xpathElementForSkipInPopup="//android.view.View[contains(@content-desc,'Log in to your Facebook account to connect to Dig Out!')and @index=1]";
     private String xpathPasswordField="//android.widget.EditText[contains(@text,'Facebook password')and @index=1]";
    private String xpathNextButton="//android.widget.Button[contains(@content-desc,'Next ')and @index=2]";
    private String partOfDefaultPopupDOM="EditText index=\"0\" text=\"Email address or phone number\" class=\"android.widget.EditText\"";
     private String partOfBigPopupDOM="class=\"android.view.View\" package=\"com.bandagames.miner\" content-desc=\"Email address or phone number\"";
     private int speedForSkip=10;
     private final static String TEST_DATA_FB_EMAIL="pettt911@gmail.com";
     private final static String TEST_DATA_FB_PASSWORD = "pettt911911";

    public AppiumDriver driver;
    public OCR ocr;
  MenuElements menuElements;
  Popups popups;
    public FBLoginPopup(AppiumDriver driver, OCR ocr,MenuElements menuElements,Popups popups){
        this.driver=driver;
        this.ocr=ocr;
        this.menuElements=menuElements;
        this.popups=popups;
    }
    @Step("open fb popup in main menu")
         public void openPopup (String button){
            ocr.clickByImage(button);
     }

    @Step("wait fb popup")
    public void waitPopup() {
            WebElement explicitLoginForm = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.EditText")));
    }
    /*
    Login methods
     */


    @Step("enter email and password")
   public void enterTestData(){
        try {

            driver.tap(1, driver.findElement(By.xpath(xpathEmailField)), 3);
            driver.getKeyboard().sendKeys(TEST_DATA_FB_EMAIL);//ввод email
            driver.tap(1, driver.findElement(By.xpath(xpathElementForSkipInPopup)), speedForSkip);//клик по точке для закрытия всплывающей клавиатуры
            driver.tap(1, driver.findElement(By.xpath(xpathPasswordField)), 3);
            driver.getKeyboard().sendKeys(TEST_DATA_FB_PASSWORD);//ввод пароля
            driver.tap(1, driver.findElement(By.xpath(xpathElementForSkipInPopup)), speedForSkip);
            driver.tap(3, driver.findElement(By.className("android.widget.Button")), 3);//клик по кнопке логина
            WebElement explicitLoginContinue = (new WebDriverWait(driver, 1000)).until(ExpectedConditions.presenceOfElementLocated(By.id(idContinuePopup)));//ожидание попапа с кнопкой continue
            driver.tap(2, driver.findElement(By.id(idContinueButton)), 3);
        }catch (NoSuchElementException e){
            new Screenshoter(driver).saveScreenshot();
        }
    }

    @Step("take localdata")
    public void enterLocalData(){//выбор локальных данных для игры
        try {
            ocr.waitUntilImageExists(popups.getConflictDataPopup(), Timeouts.getTimeoutWaitWindow());//ожидание попапа конфликт даты
            int heightWindow = driver.manage().window().getSize().getHeight();
            int widthWindow = driver.manage().window().getSize().getWidth();
            driver.tap(3, (int) (widthWindow / 2.83), (int) (heightWindow / 1.204), 10);
        }catch (TimeoutException e){
           infoNoConflictData();
        }
        ocr.waitUntilImageExists(menuElements.getLogoutButton(), Timeouts.getTimeoutWaitWindow());//ожидание что кнопка логин станет логаутом(ожидание успешности логина)
    }
     @Step("INFORM:NO CONFLICT DATA)")
     private void  infoNoConflictData(){
         System.out.println("NO CONFLICT DATA");
     }

    @Step("logout")
    public void logout() {
            ocr.clickByImage(menuElements.getLogoutButton());
            ocr.waitUntilImageExists(menuElements.getLoginButton(), 200);//ожидание ,что кнопка логаут снова станет логин(критерий успешности разлогина)
    }
 }
