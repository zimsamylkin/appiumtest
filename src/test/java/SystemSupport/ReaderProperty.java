package SystemSupport;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

public class ReaderProperty {
   static Properties settings=new Properties();
    private static JFileChooser dirObj = new JFileChooser(new File("."));
   static void  read() throws IOException {
        FileInputStream stream=new FileInputStream(dirObj.getCurrentDirectory().toString() + "/settings.properties");
        settings.load(stream);
    }
    public static String[] takeData() throws IOException {
        read();
        String deviceName="";
        String platformVersion="";
        String size="";
        String minTimeout="";
        String maxTimeout="";
        for(Map.Entry entry:settings.entrySet()){
            if(entry.getKey().equals("DEVICE_NAME")){
                deviceName=entry.getValue().toString();
            }else if (entry.getKey().equals("PLATFORM_VERSION")){
                platformVersion=entry.getValue().toString();
            } else if(entry.getKey().equals("SIZE")){
                size=entry.getValue().toString();
            } else if(entry.getKey().equals("MIN_TIMEOUT")){
                minTimeout=entry.getValue().toString();
            }
        }
        String[]params={deviceName,platformVersion,size,minTimeout};
        return params;
    }
    public static ArrayList takeDataForThreads() throws IOException {
      ArrayList paramsForThreads=new <String> ArrayList();
        BufferedReader readerParamsForThreads=new BufferedReader(new InputStreamReader(new FileInputStream(dirObj.getCurrentDirectory().toString()+"/settingsForThreads.txt")));
        String line;
        while ((line=readerParamsForThreads.readLine())!=null){
            if(!line.substring(0,5).contains("##")) {
                String PORT = line.split("PORT=")[1].split(";")[0] + " ";
                String DEVICE_NAME = line.split("DEVICE_NAME=")[1].split(";")[0] + " ";
                String PLATFORM_VERSION = line.split("PLATFORM_VERSION=")[1].split(";")[0] + " ";
                String MIN_TIMEOUT = line.split("MIN_TIMEOUT=")[1].split(";")[0] + " ";
                String SIZE=line.split("SIZE=")[1].split(";")[0]+" ";
                System.out.println("SIZE AFTER READ="+SIZE);
                System.out.println("DEVICE_NAME AFTER READ"+DEVICE_NAME);
                paramsForThreads.add(DEVICE_NAME + PLATFORM_VERSION + PORT + MIN_TIMEOUT+SIZE);
            }
        }
       return paramsForThreads;
    }
}
