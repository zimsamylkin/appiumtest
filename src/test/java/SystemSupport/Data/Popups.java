package SystemSupport.Data;


public class Popups {
    String FOLDER_SCREENS;
    public Popups(String FOLDER_SCREENS){
        this.FOLDER_SCREENS=FOLDER_SCREENS;
    }
    public  String getPickPopup() {
        return FOLDER_SCREENS+PICK_POPUP;
    }

    public   String getAchivPopup() {
        return FOLDER_SCREENS+ACHIV_POPUP;
    }

    public   String getMoneyPopup() {
        return FOLDER_SCREENS+MONEY_POPUP;
    }

    public   String getSecondPickPopup() {
        return FOLDER_SCREENS+SECOND_PICK_POPUP;
    }

    public   String getGet10GoldPopup() {
        return FOLDER_SCREENS+GET_10_GOLD_POPUP;
    }

    public   String getGet5GoldPopup() {
        return FOLDER_SCREENS+GET_5_GOLD_POPUP;
    }

    public   String getGet2GoldPopup() {
        return FOLDER_SCREENS+GET_2_GOLD_POPUP;
    }

    public   String getConflictDataPopup() {
        return FOLDER_SCREENS+CONFLICT_DATA_POPUP;
    }

    public   String getStrengthSecondPickPoppup() {
        return FOLDER_SCREENS+strengthSecondPickPoppup;
    }

    private  final  String PICK_POPUP="\\Popups\\pickPopup.png";
    private  final  String ACHIV_POPUP="\\Popups\\achivPopup.png";
    private  final  String MONEY_POPUP="\\Popups\\moneyPopup.png";
    private  final  String SECOND_PICK_POPUP="\\Popups\\secondPickPopup.png";
    private  final  String GET_10_GOLD_POPUP="\\Popups\\get10GoldPopup.png";
    private  final  String GET_5_GOLD_POPUP="\\Popups\\get5GoldPopup.png";
    private  final  String GET_2_GOLD_POPUP="\\Popups\\get2GoldPopup.png";
    private  final  String CONFLICT_DATA_POPUP="\\Popups\\windowConflict.png";
    private  final  String strengthSecondPickPoppup="\\Popups\\strengthSecondPick.png";
}
