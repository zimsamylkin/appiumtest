package SystemSupport.Data;

public class MenuElements {
     String FOLDER_SCREENS;
    public MenuElements(String FOLDER_SCREENS){
        this.FOLDER_SCREENS=FOLDER_SCREENS;
        System.out.println("MENUELEMENTS_FOLDER_SCREENS="+FOLDER_SCREENS);
    }
      public  String getPlayButton() {
        return FOLDER_SCREENS+PLAY_BUTTON;
    }

    public   String getLoginButton() {
        return FOLDER_SCREENS+LOGIN_BUTTON;
    }

    public   String getResultsButton() {
        return FOLDER_SCREENS+RESULTS_BUTTON;
    }

    public  String getAchivButton() {
        return FOLDER_SCREENS+ACHIV_BUTTON;
    }

    public  String getRatingButton() {
        return FOLDER_SCREENS+RATING_BUTTON;
    }

    public  String getSupportButton() {
        return FOLDER_SCREENS+SUPPORT_BUTTON;
    }

    public  String getVkButton() {
        return FOLDER_SCREENS+VK_BUTTON;
    }

    public  String getFbButton() {
        return FOLDER_SCREENS+FB_BUTTON;
    }

    public  String getActivePause() {
        return FOLDER_SCREENS+ACTIVE_PAUSE;
    }

    public  String getPassivePause() {
        return FOLDER_SCREENS+PASSIVE_PAUSE;
    }

    public   String getMusicOnButton() {
        return FOLDER_SCREENS+MUSIC_ON_BUTTON;
    }

    public  String getMusicOffButton() {
        return FOLDER_SCREENS+MUSIC_OFF_BUTTON;
    }

    public  String getSourceOnButton() {
        return FOLDER_SCREENS+SOURCE_ON_BUTTON;
    }

    public  String getSourceOffButton() {
        return FOLDER_SCREENS+SOURCE_OFF_BUTTON;
    }

    public   String getHomeButton() {
        return FOLDER_SCREENS+HOME_BUTTON;
    }

    public  String getLogoutButton() {
        return FOLDER_SCREENS+LOGOUT_BUTTON;
    }

    private  final  String PLAY_BUTTON = "\\MenuElements\\playbutton.png";
    private  final  String LOGIN_BUTTON ="\\MenuElements\\login.png";
    private  final  String LOGOUT_BUTTON="\\MenuElements\\logoutFB.png";
    private  final  String RESULTS_BUTTON = "\\MenuElements\\results.png";
    private    final  String ACHIV_BUTTON = "\\MenuElements\\achiv.png";
    private  final  String RATING_BUTTON = "\\MenuElements\\rating.png";
    private  final  String SUPPORT_BUTTON = "\\MenuElements\\support.png";
    private  final  String VK_BUTTON = "\\MenuElements\\vk.png";
    private  final  String FB_BUTTON = "\\MenuElements\\fb.png";
    private    final  String ACTIVE_PAUSE = "\\MenuElements\\activePause.png";
    private  final  String PASSIVE_PAUSE = "\\MenuElements\\passivePause.png";
    private  final  String MUSIC_ON_BUTTON ="\\MenuElements\\musicON.png";
    private  final  String MUSIC_OFF_BUTTON = "\\MenuElements\\musicOff.png";
    private  final  String SOURCE_ON_BUTTON = "\\MenuElements\\sourceON.png";
    private  final  String SOURCE_OFF_BUTTON = "\\MenuElements\\sourceOff.png";
    private   final  String HOME_BUTTON = "\\MenuElements\\home.png";
}
