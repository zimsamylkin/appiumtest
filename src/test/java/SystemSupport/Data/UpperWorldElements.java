package SystemSupport.Data;

public class UpperWorldElements  {
    String FOLDER_SCREENS;
    public UpperWorldElements(String FOLDER_SCREENS){
        this.FOLDER_SCREENS=FOLDER_SCREENS;
    }

    public  String getBLACKSMITH() {
        return FOLDER_SCREENS+BLACKSMITH;
    }

    public  String getOLDMAN() {
        return FOLDER_SCREENS+OLDMAN;
    }

    public  String getAchivStone() {
        return FOLDER_SCREENS+ACHIV_STONE;
    }

    public  String getBANK() {
        return FOLDER_SCREENS+BANK;
    }

    public  String getResultCup() {
        return FOLDER_SCREENS+RESULT_CUP;
    }
    public   String getChestStore() {
        return FOLDER_SCREENS+CHEST_STORE;
    }

    public   String getActiveSpeechbuble() {
        return FOLDER_SCREENS+ACTIVE_SPEECHBUBLE;
    }

    private   final  String BLACKSMITH ="\\UpperWorldElements\\blacksmith.png";
    private  final  String OLDMAN="\\UpperWorldElements\\oldman.png";
    private  final  String ACHIV_STONE="\\UpperWorldElements\\achivStone.png";
    private  final  String BANK="\\UpperWorldElements\\bank.png";
    private  final  String RESULT_CUP="\\UpperWorldElements\\cup.png";
    private  final  String CHEST_STORE="\\UpperWorldElements\\chestStore.png";
    private  final  String ACTIVE_SPEECHBUBLE="\\UpperWorldElements\\activeSpichBuble.png";
}
