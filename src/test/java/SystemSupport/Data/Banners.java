package SystemSupport.Data;

public class Banners {
    String FOLDER_SCREENS;
    public Banners(String FOLDER_SCREENS){
        this.FOLDER_SCREENS=FOLDER_SCREENS;
    }

    public  String getGoldFreeBanner() {
        return FOLDER_SCREENS+GOLD_FREE_BANNER;
    }

    public  String getFbMoneyBanner() {
        return FOLDER_SCREENS+FB_MONEY_BANNER;
    }

    public  String getBubleBirdsBunner() {
        return FOLDER_SCREENS+BUBLE_BIRDS_BUNNER;
    }

    private final  String GOLD_FREE_BANNER="\\Banners\\goldFreeBanner.png";
    private final  String FB_MONEY_BANNER="\\Banners\\fbBannerMoney.png";
    private   final   String BUBLE_BIRDS_BUNNER="\\Banners\\bubleBirdsBunner.png";
}
