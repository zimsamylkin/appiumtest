package SystemSupport.Data;


public class PanelPath  {
    String FOLDER_SCREENS;
    public PanelPath(String FOLDER_SCREENS){
       this.FOLDER_SCREENS=FOLDER_SCREENS;
    }
    public   String getSilverPanelEnterPopup() {
        return FOLDER_SCREENS+SILVER_PANEL_ENTER_POPUP;
    }

    public   String getGoldPanelEnterPopup() {
        return FOLDER_SCREENS+GOLD_PANEL_ENTER_POPUP;
    }

    public   String getPickPanelEnterPopup() {
        return FOLDER_SCREENS+PICK_PANEL_ENTER_POPUP;
    }

    private  final  String SILVER_PANEL_ENTER_POPUP="\\Panel\\silverPanel.png";
    private  final  String GOLD_PANEL_ENTER_POPUP="\\Panel\\goldPanel.png";
    private  final  String PICK_PANEL_ENTER_POPUP="\\Panel\\pickPanel.png";
}
