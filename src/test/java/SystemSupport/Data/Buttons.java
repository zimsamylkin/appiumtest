package SystemSupport.Data;

public class Buttons  {
    String FOLDER_SCREENS;
    public Buttons(String FOLDER_SCREENS){
        this.FOLDER_SCREENS=FOLDER_SCREENS;
    }
    public   String getYesButton() { return FOLDER_SCREENS+YES_BUTTON; }

    public   String getNoButton() {
        return FOLDER_SCREENS+NO_BUTTON;
    }

    public   String getUpgradeButton() {
        return FOLDER_SCREENS+UPGRADE_BUTTON;
    }

    public   String getSelectButton() {
        return FOLDER_SCREENS+SELECT_BUTTON;
    }

    public   String getSelectButtonBlackText() {
        return FOLDER_SCREENS+SELECT_BUTTON_BLACK_TEXT;
    }

    public   String getCurrentButton() {
        return FOLDER_SCREENS+CURRENT_BUTTON;
    }

    public   String getMoreOffersButton() {
        return FOLDER_SCREENS+MORE_OFFERS_BUTTON;
    }

    public   String getBackButtonWhite() {
        return FOLDER_SCREENS+BACK_BUTTON_WHITE;
    }

    private  final  String YES_BUTTON="\\Buttons\\yes.png";
    private   final  String NO_BUTTON ="\\Buttons\\no.png";
    private  final  String UPGRADE_BUTTON="\\Buttons\\upgradePick.png";
    private  final  String SELECT_BUTTON="\\Buttons\\selectButton.png";
    private  final  String SELECT_BUTTON_BLACK_TEXT="\\Buttons\\selectButtonBlackText.png";
    private  final  String CURRENT_BUTTON="\\Buttons\\currentButton.png";
    private  final  String MORE_OFFERS_BUTTON="\\Buttons\\moreOffers.png";
    private  final  String BACK_BUTTON_WHITE="\\Buttons\\buttonWhiteBack.png";
}
