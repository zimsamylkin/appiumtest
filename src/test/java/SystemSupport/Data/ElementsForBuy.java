package SystemSupport.Data;

public class ElementsForBuy {
    String FOLDER_SCREENS;
    public ElementsForBuy(String FOLDER_SCREENS){
        this.FOLDER_SCREENS=FOLDER_SCREENS;
    }

    public  String getGoldBuy40() {
        return FOLDER_SCREENS+GOLD_BUY_40;
    }

    public  String getGoldBuy75() {
        return FOLDER_SCREENS+GOLD_BUY_75;
    }

    public  String getGoldBuy150() {
        return FOLDER_SCREENS+GOLD_BUY_150;
    }

    public  String getGoldBuy450() {
        return FOLDER_SCREENS+GOLD_BUY_450;
    }

    public  String getGoldBuy1500() {
        return FOLDER_SCREENS+GOLD_BUY_1500;
    }

    public  String getGoldBuy5000() {
        return FOLDER_SCREENS+GOLD_BUY_5000;
    }

    public  String getSilverBuy100000() {
        return FOLDER_SCREENS+SILVER_BUY_100000;
    }

    public  String getSilverBuy200000() {
        return FOLDER_SCREENS+SILVER_BUY_200000;
    }

    public  String getSilverBuy750000() {
        return FOLDER_SCREENS+SILVER_BUY_750000;
    }

    public  String getSilverBuy2000000() {
        return FOLDER_SCREENS+SILVER_BUY_2000000;
    }

    public  String getSilverBuy5000000() {
        return FOLDER_SCREENS+SILVER_BUY_5000000;
    }

    public  String getSilverBuy15000000() {
        return FOLDER_SCREENS+SILVER_BUY_15000000;
    }
    private  String GOLD_BUY_40="\\ElementsForBuy\\40goldBuy.png";
    private  String GOLD_BUY_75="\\ElementsForBuy\\75goldBuy.png";
    private  String GOLD_BUY_150="\\ElementsForBuy\\150goldBuy.png";
    private  String GOLD_BUY_450="\\ElementsForBuy\\450goldBuy.png";
    private  String GOLD_BUY_1500="\\ElementsForBuy\\1500goldBuy.png";
    private  String GOLD_BUY_5000="\\ElementsForBuy\\5000goldBuy.png";
    private  String SILVER_BUY_100000="\\ElementsForBuy\\100000silverBuy.png";
    private  String SILVER_BUY_200000="\\ElementsForBuy\\200000silverBuy.png";
    private  String SILVER_BUY_750000="\\ElementsForBuy\\750000silverBuy.png";
    private  String SILVER_BUY_2000000="\\ElementsForBuy\\2000000silverBuy.png";
    private  String SILVER_BUY_5000000="\\ElementsForBuy\\5000000silverBuy.png";
    private  String SILVER_BUY_15000000="\\ElementsForBuy\\15000000silverBuy.png";
}
