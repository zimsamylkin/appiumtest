package SystemSupport.Data;

public  class Timeouts {
    public static int getTimeoutSkipPopup() {
        return timeoutSkipPopup;
    }

    public static int getTimeoutSkipMission() {
        return timeoutSkipMission;
    }

    public static int getTimeoutForSwipe() {
        return timeoutForSwipe;
    }
    public static int getTimeoutWaitWindow() {

        return timeoutWaitWindow;
    }

    public static void setTimeoutSkipPopup(int timeoutSkipPopup) {
        Timeouts.timeoutSkipPopup = timeoutSkipPopup;
    }

    public static void setTimeoutSkipMission(int timeoutSkipMission) {
        Timeouts.timeoutSkipMission = timeoutSkipMission;
    }

    public static void setTimeoutForSwipe(int timeoutForSwipe) {
        Timeouts.timeoutForSwipe = timeoutForSwipe;
    }

    public static void setTimeoutWaitWindow(int timeoutWaitWindow) {
        Timeouts.timeoutWaitWindow = timeoutWaitWindow;
    }

    private static int timeoutSkipPopup=0;//таймаут для пропуска промежуточных попапов
    private static int timeoutSkipMission=0;
    private static int  timeoutForSwipe=0;
    private static  int timeoutWaitWindow=0;
}
