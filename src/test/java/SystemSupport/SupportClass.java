package SystemSupport;

import SystemSupport.Data.Timeouts;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class SupportClass  {
     public AndroidDriver driver;
     protected AppiumDriverLocalService server;
    public OCR OCR;
  public   String PATH_TO_FULL_APK="not file build";


 public  void getAPKName(){//получаем путь до билда внутри папки проекта
     JFileChooser dirObj = new JFileChooser(new File("."));
      String directoryPath=dirObj.getCurrentDirectory().toString()+"/build";
     File dir=new File(directoryPath);
     if(dir.isDirectory()){
         for (File item:dir.listFiles()){
             PATH_TO_FULL_APK=directoryPath+"/"+item.getName();
         }
     }
     System.out.println("PATH TO FULL APK="+PATH_TO_FULL_APK);
 }
    protected void serverDefaultStart(){//запуск аппиум-сервера через 0000
         server=AppiumDriverLocalService.buildDefaultService();
         server.start();
     }

    public void serverStart(int port){//запуск сервера через свои параметры
         server=AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
         .usingPort(port));
         server.start();
     }

    protected void initDriver(String[]params,String url) throws MalformedURLException {//prepare param for android driver
        DesiredCapabilities capabilities = new DesiredCapabilities();
        File app=new File(PATH_TO_FULL_APK);
        capabilities.setCapability("appPackage", "com.bandagames.miner");
        capabilities.setCapability("platform","ANDROID");
        capabilities.setCapability("app_activity","app_activity");
        capabilities.setCapability("version", params[1]);
        capabilities.setCapability("deviceName", params[0]);
        capabilities.setCapability("app", app.getAbsolutePath());

        driver = new AndroidDriver(new URL(url), capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 10);

        //Sikuli settings
        OCR = new OCR(driver);

        //location of screenshots
        File classpathRoot = new File(System.getProperty("user.dir"));
        File imgDir = new File(classpathRoot, "src/main/resources");
        //location of screens for work ocr
        System.out.println("SIZE="+params[4]);
        //init timeouts
        Timeouts.setTimeoutSkipPopup(Integer.parseInt(params[3]));
        Timeouts.setTimeoutWaitWindow(Integer.parseInt(params[3]));
        Timeouts.setTimeoutForSwipe(Integer.parseInt(params[3])+Integer.parseInt(params[3])/2);
        Timeouts.setTimeoutSkipMission(6*Integer.parseInt(params[3]));

    }
    protected void quitDriver(){//exit in driver
        driver.quit();
    }

}
