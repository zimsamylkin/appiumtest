package SystemSupport;

import io.appium.java_client.AppiumDriver;
import org.aspectj.util.FileUtil;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Screenshoter  {
    private AppiumDriver driver;
    public Screenshoter(AppiumDriver driver){
        this.driver=driver;
    }
    /*
    Создаем сущность для скринов.Сюда обращаемся со всего кода
     */
    @Attachment(value="{0}",type="image/png")
    public byte[] saveScreenshot(){
        String destDir = "screenshots";
        File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
        new File(destDir).mkdirs();
        String destFile = dateFormat.format(new Date()) + ".png";
        try {
            // Copy paste file at destination folder location
            FileUtil.copyFile(file, new File(destDir + "/" + destFile));
            return toByteArray(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
    private static byte[]toByteArray(File file) throws IOException {
        return Files.readAllBytes(Paths.get(file.getPath()));
    }
}